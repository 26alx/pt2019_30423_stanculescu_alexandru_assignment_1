package Test;

import operands.*;
import operations.ops;
import junit.framework.TestCase;

import org.junit.Test;

public class TestClass extends TestCase {
	private Polynom p1;
	private Polynom p2;
	private Polynom p3;
	private Polynom p4;
	private Polynom p5;
	private Polynom p6;
	private Polynom p7;
	private String firstPolynom = "x^2+x+1";
	private String secondPolynom = "2x";

	public TestClass() {

		p3 = new Polynom();
		p4 = new Polynom();
		p5 = new Polynom();
		p6 = new Polynom();
		p7 = new Polynom();
	}
	@Test
	public void tests() throws Exception {
		p1 = new Polynom(firstPolynom);
		p2 = new Polynom(secondPolynom);
	/*	p3 = ops.add(p1, p2);
		System.out.println(p3.printPoly());
		assertTrue(p3.printPoly().equals("X^2+3.0X+1.0"));
		p4 = ops.sub(p1, p2);
		System.out.println(p4.printPoly() + "\n" + p2.printPoly());
		assertTrue(p4.printPoly().equals("X^2-X+1.0"));
		p5 = ops.mult(p1, p2);
		System.out.println(p5.printPoly());
		assertTrue(p5.printPoly().equals("2.0X^3+2.0X^2+2.0X"));
		p6 = ops.derive(p1,0);
		System.out.println(p6.printPoly());
		assertTrue(p6.printPoly().equals("2.0X+1.0"));*/
		System.out.println(p2.printPoly());
		p7 = ops.derive(p2,1);
		System.out.println(p7.printPoly());
		assertTrue(p7.printPoly().equals("+X^2"));
		System.out.println(p2.printPoly());

	}
}