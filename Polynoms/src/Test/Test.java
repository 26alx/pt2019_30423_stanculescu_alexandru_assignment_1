package Test;

import operands.*;
import operations.ops;

public class Test {
	public static void main(String[] args) {
		Polynom a, b, c;
		String polynom1 = "-3-4x^2+x^3+2*x", polynom2 = "+2", p;
		p = polynom1.replace("*","");
		//System.out.println(p);
		a = new Polynom(p);
		b = new Polynom(polynom2);
		//a.printPoly();
	//	b.printPoly();
	//	System.out.println(a.printPoly());
	//	System.out.println(b.printPoly() + " BBB");
		c = ops.add(a, b);
		System.out.println(c.printPoly());
	}
}
