package design;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import operands.*;
import operations.*;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class frontend {

	private JFrame frame;
	private JLabel lblPolynom1;
	private JLabel lblPolynom2;
	private JLabel lblPolynom3;
	private JTextField textFieldPolynom1;
	private JTextField textFieldPolynom2;
	private JTextField textFieldPolynom3;
	private JButton btnadd;
	private JButton btnsub;
	private JButton btnmult;
	private JButton btndiv;
	private JButton btnderive;
	private JButton btnintegr;
	private Polynom a;
	private Polynom b;
	private Polynom c = new Polynom();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frontend window = new frontend();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public frontend() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		lblPolynom1 = new JLabel("Polynomial a:");
		lblPolynom2 = new JLabel("Polynomial b:");
		lblPolynom3 = new JLabel("Result: ");
		textFieldPolynom1 = new JTextField();
		textFieldPolynom2 = new JTextField();
		textFieldPolynom3 = new JTextField();
		btnadd = new JButton("+");
		btnsub = new JButton("-");
		btnmult = new JButton("*");
		btndiv = new JButton("/");
		btnderive = new JButton("f'(a)");
		btnintegr = new JButton("f^-1(a)");

		frame.setBounds(100, 100, 550, 400);
		lblPolynom1.setBounds(10, 10, 85, 15);
		lblPolynom2.setBounds(10, 40, 85, 15);
		lblPolynom3.setBounds(10, 70, 85, 15);
		textFieldPolynom1.setBounds(95, 10, 400, 15);
		textFieldPolynom1.setColumns(30);
		textFieldPolynom2.setBounds(95, 40, 400, 15);
		textFieldPolynom2.setColumns(30);
		textFieldPolynom3.setBounds(95, 70, 400, 15);
		textFieldPolynom3.setColumns(30);
		textFieldPolynom3.setEditable(false);
		textFieldPolynom3.setText("");
		btnadd.setBounds(95, 160, 90, 20);
		btnsub.setBounds(195, 160, 90, 20);
		btnmult.setBounds(295, 160, 90, 20);
		btndiv.setBounds(395, 160, 90, 20);
		btnderive.setBounds(95, 200, 90, 20);
		btnintegr.setBounds(395, 200, 90, 20);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(lblPolynom1);
		frame.getContentPane().add(lblPolynom2);
		frame.getContentPane().add(lblPolynom3);
		frame.getContentPane().add(textFieldPolynom1);
		frame.getContentPane().add(textFieldPolynom2);
		frame.getContentPane().add(textFieldPolynom3);
		frame.getContentPane().add(btnadd);
		frame.getContentPane().add(btnsub);
		frame.getContentPane().add(btnmult);
		frame.getContentPane().add(btndiv);
		frame.getContentPane().add(btnderive);
		frame.getContentPane().add(btnintegr);
		
		btnadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				a = new Polynom(textFieldPolynom1.getText().replace("*",""));
				b = new Polynom(textFieldPolynom2.getText().replace("*",""));
				c = ops.add(a, b);
				textFieldPolynom3.setText(c.printPoly());
			}
		});
		
		btnsub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				a = new Polynom(textFieldPolynom1.getText().replace("*",""));
				b = new Polynom(textFieldPolynom2.getText().replace("*",""));
				c = ops.sub(a, b);
				textFieldPolynom3.setText(c.printPoly());
			}
		});
		
		btnmult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				a = new Polynom(textFieldPolynom1.getText().replace("*",""));
				b = new Polynom(textFieldPolynom2.getText().replace("*",""));
				c = ops.mult(a, b);
				textFieldPolynom3.setText(c.printPoly());
			}
		});
		
		btndiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPolynom3.setText("This feature is not yet implemented!");
			}
		});
		
		btnderive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				a = new Polynom(textFieldPolynom1.getText().replace("*",""));
				b = new Polynom(textFieldPolynom2.getText().replace("*",""));
				c = ops.derive(a, 0);
				textFieldPolynom3.setText(c.printPoly());
			}
		});
		
		btnintegr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				a = new Polynom(textFieldPolynom1.getText().replace("*",""));
				b = new Polynom(textFieldPolynom2.getText().replace("*",""));
				c = ops.derive(a, 1);
				textFieldPolynom3.setText(c.printPoly());
			}
		});
	}
}
