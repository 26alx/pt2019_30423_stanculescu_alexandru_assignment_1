package operands;

public class Monom {

	private int power;
	private double coeff;

	public Monom(double coeff, int power) {
		this.setCoeff(coeff);
		this.setPower(power);
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public double getCoeff() {
		return coeff;
	}

	public void setCoeff(double coeff) {
		this.coeff = coeff;
	}

	
}
