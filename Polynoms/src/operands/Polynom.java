package operands;

import java.util.ArrayList;
import java.util.Collections;
//import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynom {
	private ArrayList<Monom> monom;
	private int pow = 0, maxpow = 0;
	private double coeff = 0.0;
	private String poly;

	public Polynom() {
		monom = new ArrayList<Monom>();
	}

	public Polynom(String poly) {
		monom = new ArrayList<Monom>();
		Pattern pattern = Pattern.compile("([+-]?\\d*)x(\\^(\\d)|\\d)?|([+-]\\d+|\\d)");
		Matcher matcher = pattern.matcher(poly);
		while (matcher.find()) {
			if (matcher.group(4) == null) {
				pow = (matcher.group(3) == null) ? 1 : Integer.parseInt(matcher.group(3));
				if (pow > maxpow)
					maxpow = pow;
				if (matcher.group(1).equals("-")) {
					coeff = -1;
				} else {
					if (matcher.group(1).equals("+") || matcher.group(1).equals("")) {
						coeff = 1;
					} else if (matcher.group(1) != "0")
						coeff = Double.parseDouble(matcher.group(1));
				}
			} else {
				pow = 0;
				if (matcher.group(4) != "0")
					coeff = Double.parseDouble(matcher.group(4));
			}
			Monom mon = new Monom(coeff, pow);
			monom.add(mon);
		}
		order(monom);
	}

	public ArrayList<Monom> get_monoms() {
		return monom;
	}

	private void order(ArrayList<Monom> monom) {
		int i, j;
		for (i = 0; i < monom.size() - 1; i++) {
			for (j = i + 1; j < monom.size(); j++) {
				if (monom.get(j).getCoeff() == 0.0)
					monom.remove(j);
				if (monom.get(i).getPower() == monom.get(j).getPower()) {
					monom.get(i).setCoeff(monom.get(j).getCoeff());
					monom.remove(j);
					if (monom.get(i).getCoeff() == 0)
						monom.remove(i);
				} else if (monom.get(i).getPower() < monom.get(j).getPower()) {
					Collections.swap(monom, i, j);
				}
				if (monom.get(i).getPower() > maxpow)
					maxpow = monom.get(i).getPower();
			}
		}
	}

	public String printPoly() {
		order(monom);
		poly = "";
		monom.forEach((n) -> {
			if (n.getCoeff() == 1.0) {
				if (n.getPower() == 1)
					poly += "+X";
				else if (n.getPower() == maxpow)
					poly += "X^" + n.getPower();
				else if (n.getPower() == 0)
					poly += "+1.0";
				else
					poly += "+X^" + n.getPower();
			} else if (n.getCoeff() == -1.0) {
				if (n.getPower() == 1)
					poly += "-X";
				else if (n.getPower() == maxpow)
					poly += "X^" + n.getPower();
				else if (n.getPower() == 0)
					poly += "-1.0";
				else
					poly += "-X^" + n.getPower();
			} else {
				poly += (n.getCoeff() > 0 && n.getPower() != maxpow) ? "+" + n.getCoeff() : n.getCoeff();
				poly += (n.getPower() > 1) ? "X^" + n.getPower() : (n.getPower() == 1) ? "X" : "";
			}
		});
		return poly;
	}
}
