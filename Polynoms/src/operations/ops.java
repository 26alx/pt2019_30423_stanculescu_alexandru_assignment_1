package operations;

import operands.*;

public class ops {
	public static Polynom add(Polynom a, Polynom b) {
		int la = a.get_monoms().size(), lb = b.get_monoms().size(), i = 0, j = 0;
		double coeff;
		Polynom c = new Polynom();
		Monom m;
		while (i < la && j < lb) {
			if (a.get_monoms().get(i).getPower() == b.get_monoms().get(j).getPower()) {
				coeff = a.get_monoms().get(i).getCoeff() + b.get_monoms().get(j).getCoeff();
				if (coeff != 0.0) {
					m = new Monom(coeff, a.get_monoms().get(i).getPower());
					c.get_monoms().add(m);
				}
				i++;
				j++;
			} else if (a.get_monoms().get(i).getPower() > b.get_monoms().get(j).getPower()) {
				m = new Monom(a.get_monoms().get(i).getCoeff(), a.get_monoms().get(i).getPower());
				c.get_monoms().add(m);
				i++;
			} else {
				m = new Monom(b.get_monoms().get(j).getCoeff(), b.get_monoms().get(j).getPower());
				c.get_monoms().add(m);
				j++;
			}
		}
		while (i < la) {
			m = new Monom(a.get_monoms().get(i).getCoeff(), a.get_monoms().get(i).getPower());
			c.get_monoms().add(m);
			i++;
		}
		while (j < lb) {
			m = new Monom(b.get_monoms().get(j).getCoeff(), b.get_monoms().get(j).getPower());
			c.get_monoms().add(m);
			j++;
		}
		return c;
	}

	public static Polynom sub(Polynom a, Polynom b) {
		Polynom c = new Polynom();
		for (Monom m : b.get_monoms())
			c.get_monoms().add(m);
		for (Monom mc : c.get_monoms())
			mc.setCoeff(-1 * mc.getCoeff());
		return add(a, c);
	}

	public static Polynom mult(Polynom a, Polynom b) {
		Polynom d = new Polynom();
		Monom mm;
		for (Monom mc : a.get_monoms())
			for (Monom mb : b.get_monoms()) {
				mm = new Monom(mc.getCoeff() * mb.getCoeff(),mc.getPower() + mb.getPower());
				d.get_monoms().add(mm);
				System.out.println(d.printPoly());
			}
		return d;
	}

	public static Polynom derive(Polynom a, int mode) {
		Polynom b = a, c = new Polynom();
		for (Monom mb : b.get_monoms()) {
			if (mode == 0) {
				mb.setCoeff(mb.getCoeff() * mb.getPower());
				mb.setPower(mb.getPower() - 1);
			} else {
				mb.setPower(mb.getPower() + 1);
				mb.setCoeff(mb.getCoeff() * (1.0 / mb.getPower()));
			}
			if (mb.getCoeff() != 0.0 && mb.getCoeff() != -0.0) {
				c.get_monoms().add(mb);
			}
		}
		return c;
	}

	private static void rest(Polynom rest) {
		rest.printPoly();
	}

	public static Polynom div(Polynom a, Polynom b) {
		Polynom c = a, d = new Polynom();
		rest(c);
		return d;
	}

}
